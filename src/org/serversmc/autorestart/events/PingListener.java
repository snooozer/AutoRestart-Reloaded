package org.serversmc.autorestart.events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.serversmc.autorestart.core.Main;
import org.serversmc.autorestart.core.TimerThread;
import org.serversmc.autorestart.utils.Config;

public class PingListener implements Listener {
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onServerListPing(ServerListPingEvent event) {
		if (!Config.UPDATEMOTD.ENABLED()) {
			return;
		}
		
		TimerThread timerThread = Main.timerThread;
		if (timerThread.running) {
			if(timerThread.time < Config.UPDATEMOTD.SHOW_AT_REMAINING_SECONDS()){
				String pingMessage = Config.UPDATEMOTD.MOTD_MESSAGE_FORMAT().replace("%d", String.valueOf(timerThread.time));
				event.setMotd(ChatColor.translateAlternateColorCodes((char) '&', (String) pingMessage));
			}
		}
	}
}