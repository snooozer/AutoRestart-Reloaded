#!/bin/sh
while $RELOAD
do
    RELOAD=false
    java -Xmx4096M -Xms1024M -jar spigot.jar -o true
    if [ -f plugins/AutoRestart/restart.txt ]; then
        RELOAD=true
        echo "Restarting server in 5 seconds..."
        sleep 5
    fi
done