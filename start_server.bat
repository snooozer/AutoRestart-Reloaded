@echo off
:start
java -Xmx4096M -Xms1024M -XX:MaxPermSize=128M -jar spigot.jar -o true
if exists plugins\AutoRestart\restart.txt (
    echo "Restarting server in 5 seconds..."
    timeout /t 5 /nobreak > NUL
    goto start
)
pause